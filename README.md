A library for accessing the Chrome APIs in a packaged app or extension.

## Building examples

```bash
cd ./examples/ext_test
pub run build_runner build --config debug --output build
```