export 'package:chrome/src/ext/chrome.dart';

// for context menus
export 'package:chrome/src/ext/context_menu.dart'
    show
        CreateContextMenusProperties,
        ItemType,
        ContextMenus,
        UpdateContextMenusProperties;

export 'package:chrome/src/ext/interop/context_menu.dart'
    show actionMenuTopLevelLimit, ContextMenusPropertiesOnClickData;

// for notifications
export 'package:chrome/src/ext/notification.dart'
    show
        CreatNotificationOptions,
        NotificationTemplateType,
        NotificationPermissionLevel,
        Button,
        Item;
