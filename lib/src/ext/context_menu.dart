import 'dart:async';
import 'dart:js';
import 'package:chrome/src/utils/enum.dart';
import 'package:chrome/src/ext/interop/context_menu.dart' as interop;

/// The different contexts a menu can appear in.
/// Specifying 'all' is equivalent to the combination of all other contexts except for 'launcher'.
/// The 'launcher' context is only supported by apps and is used to add menu items to the context
/// menu that appears when clicking on the app icon in the launcher/taskbar/dock/etc.
/// Different platforms might put limitations on what is actually supported in a launcher context menu.
class ContextType extends Enum<String> {
  static const all = const ContextType._('all');
  static const page = const ContextType._('page');
  static const frame = const ContextType._('frame');
  static const selection = const ContextType._('selection');
  static const link = const ContextType._('link');
  static const editable = const ContextType._('editable');
  static const image = const ContextType._('image');
  static const video = const ContextType._('video');
  static const audio = const ContextType._('audio');
  static const launcher = const ContextType._('launcher');
  static const browserAction = const ContextType._('browser_action');
  static const pageAction = const ContextType._('page_action');

  const ContextType._(String value) : super(value);
}

/// The type of menu item.
class ItemType extends Enum<String> {
  static const normal = const ItemType._('normal');
  static const checkbox = const ItemType._('checkbox');
  static const radio = const ItemType._('radio');
  static const separator = const ItemType._('separator');

  const ItemType._(String value) : super(value);
}

typedef void ContextMenusPropertiesOnClick(
    interop.ContextMenusPropertiesOnClickData info, tab);

class ContextMenusOnClickData {
  final interop.ContextMenusPropertiesOnClickData info;
  final tab;

  ContextMenusOnClickData(this.info, this.tab);
}

class UpdateContextMenusProperties {
  final ItemType type;
  final String title;
  final bool checked;
  final List<ContextType> contexts;
  final bool visible;
  final parentId;
  final List<String> documentUrlPatterns;
  final List<String> targetUrlPatterns;
  final bool enabled;

  final _onClickController = StreamController<ContextMenusOnClickData>();

  UpdateContextMenusProperties(
      {this.type,
      this.title,
      this.checked,
      this.contexts,
      this.visible,
      /* int | String */ this.parentId,
      this.documentUrlPatterns,
      this.targetUrlPatterns,
      this.enabled});

  Stream<ContextMenusOnClickData> get onClick => _onClickController.stream;

  void dispose() {
    _onClickController.close();
  }

  void _onClick(interop.ContextMenusPropertiesOnClickData info, tab) {
    _onClickController.add(ContextMenusOnClickData(info, tab));
  }

  interop.UpdateContextMenusProperties _toInterop() =>
      interop.UpdateContextMenusProperties(
        type: type.value,
        title: title,
        checked: checked,
        contexts: contexts?.map((context) => context.value)?.toList(),
        visible: visible,
        onclick: allowInterop(_onClick),
        parentId: parentId,
        documentUrlPatterns: documentUrlPatterns,
        targetUrlPatterns: targetUrlPatterns,
        enabled: enabled,
      );
}

class CreateContextMenusProperties {
  final String id;
  final ItemType type;
  final String title;
  final bool checked;
  final List<ContextType> contexts;
  final bool visible;
  final parentId;
  final List<String> documentUrlPatterns;
  final List<String> targetUrlPatterns;
  final bool enabled;

  final _onClickController = StreamController<ContextMenusOnClickData>();

  CreateContextMenusProperties(
      {this.id,
      this.type,
      this.title,
      this.checked,
      this.contexts,
      this.visible,
      /* int | String */ this.parentId,
      this.documentUrlPatterns,
      this.targetUrlPatterns,
      this.enabled});

  Stream<ContextMenusOnClickData> get onClick => _onClickController.stream;

  void dispose() {
    _onClickController.close();
  }

  void _onClick(interop.ContextMenusPropertiesOnClickData info, tab) {
    _onClickController.add(ContextMenusOnClickData(info, tab));
  }

  @override
  interop.CreateContextMenusProperties _toInterop() =>
      interop.CreateContextMenusProperties(
        type: type.value,
        id: id,
        title: title,
        checked: checked,
        contexts: contexts?.map((context) => context.value)?.toList(),
        visible: visible,
        onclick: allowInterop(_onClick),
        parentId: parentId,
        documentUrlPatterns: documentUrlPatterns,
        targetUrlPatterns: targetUrlPatterns,
        enabled: enabled,
      );
}

class ContextMenus {
  interop.ContextMenus _contextMenus;

  ContextMenus.fromInterop(this._contextMenus);

  Future /* int | String */ create(
      CreateContextMenusProperties createProperties) async {
    final _properties = createProperties._toInterop();
    final completer = new Completer<void>();
    final result =
        _contextMenus.create(_properties, allowInterop(completer.complete));
    await completer.future;
    return result;
  }

  Future /* int | String */ update(/* int | String */ id,
      UpdateContextMenusProperties updateProperties) async {
    final _properties = updateProperties._toInterop();
    final completer = new Completer<void>();
    final result =
        _contextMenus.update(id, _properties, allowInterop(completer.complete));
    await completer.future;
    return result;
  }
}
