import 'dart:async';
import 'package:chrome/src/utils/enum.dart';
import 'package:chrome/src/ext/interop/notification.dart' as interop;
import 'dart:js';

/// Which type of notification to display.
/// Required for notifications.create method.
class NotificationTemplateType extends Enum<String> {
  /// icon, title, message, expandedMessage, up to two buttons
  static const basic = const NotificationTemplateType._('basic');

  /// icon, title, message, expandedMessage, image, up to two buttons
  static const image = const NotificationTemplateType._('image');

  /// icon, title, message, items, up to two buttons.
  /// Users on Mac OS X only see the first item.
  static const list = const NotificationTemplateType._('list');

  /// icon, title, message, progress, up to two buttons
  static const progress = const NotificationTemplateType._('progress');

  const NotificationTemplateType._(String value) : super(value);
}

/// Whether the user has enabled notifications from this app or extension.
class NotificationPermissionLevel extends Enum<String> {
  /// User has elected to show notifications from the app or extension.
  /// This is the default at install time.
  static const granted = const NotificationPermissionLevel._('granted');

  /// User has elected not to show notifications from the app or extension.
  static const denied = const NotificationPermissionLevel._('denied');

  const NotificationPermissionLevel._(String value) : super(value);
}

/// Buttons displayed on notification popup
class Button {
  /// Required
  final String title;

  /// Optional
  final String iconUrl;

  Button({
    this.title,
    this.iconUrl,
  });
}

/// Items for multi-item notifications.
class Item {
  /// Title of one item of a list notification.
  final String title;

  /// Additional details about this item.
  final String message;

  Item({
    this.title,
    this.message,
  });
}

/// Options to be used while creating new notifications
class CreatNotificationOptions {
  /// Which type of notification to display.
  /// Required for notifications.create method.
  final NotificationTemplateType type;

  /// A URL to the sender's avatar, app icon, or a thumbnail for image notifications.
  /// URLs can be a data URL, a blob URL, or a URL relative
  /// to a resource within this extension's
  /// .crx file Required for notifications.create method.
  final String iconUrl;

  /// Title of the notification (e.g. sender name for email).
  /// Required for notifications.create
  /// method.
  final String title;

  /// Main notification content. Required for notifications.create method.
  final String message;

  /// Since Chrome 31.
  /// Alternate notification content with a lower-weight font.
  final String contextMessage;

  /// Priority ranges from -2 to 2. -2 is lowest priority. 2 is highest. Zero is default.
  /// On platforms that don't support a notification center (Windows, Linux & Mac),
  /// -2 and -1 result in an error as notifications with those priorities will not be shown at all.
  final int priority;

  /// A timestamp associated with the notification,
  /// in milliseconds past the epoch (e.g. Date.now() + n).
  final double eventTime;

  /// Text and icons for up to two notification action buttons.
  final List<Button> buttons;

  /// Items for multi-item notifications.
  /// Users on Mac OS X only see the first item.
  final List<Item> items;

  /// Current progress ranges from 0 to 100.
  final int progress;

  /// Indicates that the notification should remain visible on screen until
  /// the user activates or dismisses the notification. This defaults to false.
  final bool requireInteraction;

  CreatNotificationOptions({
    this.type,
    this.iconUrl,
    this.title,
    this.message,
    this.contextMessage,
    this.priority,
    this.eventTime,
    this.buttons,
    this.items,
    this.progress,
    this.requireInteraction,
  });
  interop.NotificationOptions _toInterop() {
    return interop.NotificationOptions(
      type: type.value,
      iconUrl: iconUrl,
      title: title,
      message: message,
      contextMessage: contextMessage,
      priority: priority,
      eventTime: eventTime,
      buttons: buttons?.map((button) {
        print(button.toString());
        return interop.Button(
          title: (button.title != null) ? button.title : "",
          iconUrl: (button.iconUrl) != null ? button.iconUrl : "",
        );
      })?.toList(),
      items: items?.map((item) {
        print(item);
        return interop.Item(
          title: (item.title != null) ? item.title : "",
          message: (item.message != null) ? item.message : "",
        );
      })?.toList(),
      progress: progress,
      requireInteraction: requireInteraction,
    );
  }
}

class Notifications {
  interop.Notifications _notification;
  Notifications.fromInterop(this._notification);

  Future /* int | String */ create(
      String notificationId, CreatNotificationOptions options) async {
    print(_notification);
    final _properties = options._toInterop();
    final completer = new Completer<void>();
    final result = _notification.create(
        notificationId, _properties, allowInterop(completer.complete));
    await completer.future;
    return result;
  }
}

/*
/// Buttons displayed on notification popup
class Button {
  /// Required
  final String title;

  /// Optional
  final String iconURL;
}

/// Items for multi-item notifications.
class Items {
  /// Title of one item of a list notification.
  final String title;

  /// Additional details about this item.
  final String message;
}
*/
