@JS()
library chrome;

import 'package:js/js.dart';
import 'package:chrome/src/ext/interop/context_menu.dart'
    as interop_context_menus;

import 'package:chrome/src/ext/interop/notification.dart'
    as interop_notification;

@JS('chrome')
external Chrome get chrome;

@JS()
@anonymous
class Chrome {
  external interop_context_menus.ContextMenus get contextMenus;

  external interop_notification.Notifications get notifications;
}
