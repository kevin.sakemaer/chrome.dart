@JS()
library chrome.ext.context_menu;

import 'package:js/js.dart';

/// The maximum number of top level extension items that can be added to an extension action context menu.
///  Any items beyond this limit will be ignored.
@JS('chrome.contextMenus.ACTION_MENU_TOP_LEVEL_LIMIT')
external int get actionMenuTopLevelLimit;

@JS()
@anonymous
class ContextMenusPropertiesOnClickData {
  external factory ContextMenusPropertiesOnClickData(
      {/* int | String */ menuItemId,
      /* int | String */ parentMenuItemId,
      String mediaType,
      String linkUrl,
      String srcUrl,
      String pageUrl,
      String frameUrl,
      int frameId,
      String selectionText,
      bool editable,
      bool wasChecked,
      bool checked});

  external /* int | String */ get menuItemId;
  external /* int | String */ get parentMenuItemId;
  external String get mediaType;
  external String get linkUrl;
  external String get srcUrl;
  external String get pageUrl;
  external String get frameUrl;
  external int get frameId;
  external String get selectionText;
  external bool get editable;
  external bool get wasChecked;
  external bool get checked;

  //  TODO: we don't support tab right now
  // external Tab get tab;
}

@JS()
@anonymous
class CreateContextMenusProperties {
  external factory CreateContextMenusProperties(
      {String type,
      String id,
      String title,
      bool checked,
      List<String> contexts,
      bool visible,
      Function onclick,
      /* int | String */ parentId,
      List<String> documentUrlPatterns,
      List<String> targetUrlPatterns,
      bool enabled});

  external String get type;
  external String get id;
  external String get title;
  external bool get checked;
  external List<String> get contexts;
  external bool get visible;
  external Function get onclick;
  external /* int | String */ get parentId;
  external List<String> get documentUrlPatterns;
  external List<String> get targetUrlPatterns;
  external bool get enabled;
}

@JS()
@anonymous
class UpdateContextMenusProperties {
  external factory UpdateContextMenusProperties(
      {String type,
      String title,
      bool checked,
      List<String> contexts,
      bool visible,
      Function onclick,
      /* int | String */ parentId,
      List<String> documentUrlPatterns,
      List<String> targetUrlPatterns,
      bool enabled});

  external String get type;
  external String get title;
  external bool get checked;
  external List<String> get contexts;
  external bool get visible;
  external Function get onclick;
  external /* int | String */ get parentId;
  external List<String> get documentUrlPatterns;
  external List<String> get targetUrlPatterns;
  external bool get enabled;
}

@JS('chrome.contextMenus')
class ContextMenus {
  /// Creates a new context menu item.
  /// Note that if an error occurs during creation, you may not find out until the creation callback fires (the details will be in chrome.runtime.lastError).
  external /* int | String */ create(
      CreateContextMenusProperties createProperties,
      [Function callback]);

  external /* int | String */ update(
      /* int | String */ id, UpdateContextMenusProperties updateProperties,
      [Function callback]);
}
