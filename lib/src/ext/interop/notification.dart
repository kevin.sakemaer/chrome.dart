@JS()
library chrome;

import 'package:js/js.dart';

/// Buttons displayed on notification popup
@JS()
@anonymous
class Button {
  /// Required
  external String get title;

  /// Optional
  external String get iconUrl;

  external factory Button({
    String title,
    String iconUrl,
  });
}

/// Items for multi-item notifications.
@JS()
@anonymous
class Item {
  /// Title of one item of a list notification.
  external String get title;

  /// Additional details about this item.
  external String get message;

  external factory Item({
    String title,
    String message,
  });
}

@JS()
@anonymous
class NotificationOptions {
  external factory NotificationOptions({
    /// Which type of notification to display.
    /// Required for notifications.create method.
    String type,

    /// A URL to the sender's avatar, app icon, or a thumbnail for image notifications.
    /// URLs can be a data URL, a blob URL, or a URL relative
    /// to a resource within this extension's
    /// .crx file Required for notifications.create method.
    String iconUrl,

    /// Title of the notification (e.g. sender name for email).
    /// Required for notifications.create
    /// method.
    String title,

    /// Main notification content. Required for notifications.create method.
    String message,

    /// Since Chrome 31.
    /// Alternate notification content with a lower-weight font.
    String contextMessage,

    /// Priority ranges from -2 to 2. -2 is lowest priority. 2 is highest. Zero is default.
    /// On platforms that don't support a notification center (Windows, Linux & Mac),
    /// -2 and -1 result in an error as notifications with those priorities will not be shown at all.
    int priority,

    /// A timestamp associated with the notification,
    /// in milliseconds past the epoch (e.g. Date.now() + n).
    double eventTime,

    /// Text and icons for up to two notification action buttons.
    List<Button> buttons,

    /// Items for multi-item notifications.
    /// Users on Mac OS X only see the first item.
    List<Item> items,

    /// Current progress ranges from 0 to 100.
    int progress,

    /// Indicates that the notification should remain visible on screen until
    /// the user activates or dismisses the notification. This defaults to false.
    bool requireInteraction,
  });

  /// Which type of notification to display.
  /// Required for notifications.create method.
  external String get type;

  /// A URL to the sender's avatar, app icon, or a thumbnail for image notifications.
  /// URLs can be a data URL, a blob URL, or a URL relative
  /// to a resource within this extension's
  /// .crx file Required for notifications.create method.
  external String get iconUrl;

  /// Title of the notification (e.g. sender name for email).
  /// Required for notifications.create
  /// method.
  external String get title;

  /// Main notification content. Required for notifications.create method.
  external String get message;

  /// Since Chrome 31.
  /// Alternate notification content with a lower-weight font.
  external String get contextMessage;

  /// Priority ranges from -2 to 2. -2 is lowest priority. 2 is highest. Zero is default.
  /// On platforms that don't support a notification center (Windows, Linux & Mac),
  /// -2 and -1 result in an error as notifications with those priorities will not be shown at all.
  external int get priority;

  /// A timestamp associated with the notification,
  /// in milliseconds past the epoch (e.g. Date.now() + n).
  external double get eventTime;

  /// Text and icons for up to two notification action buttons.
  external List<Button> get buttons;

  /// Items for multi-item notifications.
  /// Users on Mac OS X only see the first item.
  external List<Item> get items;

  /// Current progress ranges from 0 to 100.
  external int get progress;

  /// Indicates that the notification should remain visible on screen until
  /// the user activates or dismisses the notification. This defaults to false.
  external bool get requireInteraction;
}

/// Placeholder for methods related to showing notifications
@JS('chrome.notifications')
class Notifications {
  external dynamic create(String notificationId, NotificationOptions options,
      [Function callback]);
/*
  TODO : not implemented
  update − chrome.notifications.update(string notificationId, NotificationOptions options, function callback)
  clear − chrome.notifications.clear(string notificationId, function callback)
  getAll − chrome.notifications.getAll(function callback)
  getPermissionLevel −
*/
}
