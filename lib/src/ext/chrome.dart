import 'package:chrome/src/ext/interop/chrome.dart' as interop_chrome;
import 'package:chrome/src/ext/context_menu.dart';
import 'package:chrome/src/ext/notification.dart';

final chrome = new _Chrome._fromInterop(interop_chrome.chrome);

class _Chrome {
  interop_chrome.Chrome _chrome;

  _Chrome._fromInterop(this._chrome);

  ContextMenus get contextMenus =>
      ContextMenus.fromInterop(_chrome.contextMenus);

  Notifications get notifications =>
      Notifications.fromInterop(_chrome.notifications);
}
