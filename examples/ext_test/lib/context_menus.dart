import 'package:chrome/chrome.dart' as c;
import 'dart:html' as html;

/// to test context menu apis

void contextMenuTest() async {
  // menu item 1
  final menuItem1 = c.CreateContextMenusProperties(
    type: c.ItemType.normal,
    id: 'menu_item_1',
    title: 'menu item 1',
  );
  menuItem1.onClick.listen((data) {
    html.window.alert('Menu item 1 clicked');
  });
  final result = await c.chrome.contextMenus.create(menuItem1);
  print(result);
  // menu item 2
  final menuItem2 = c.CreateContextMenusProperties(
    type: c.ItemType.normal,
    id: 'menu_item_2',
    title: 'menu item 2',
  );
  menuItem2.onClick.listen((data) {
    html.window.alert('Menu item 2 clicked');
  });
  final result2 = await c.chrome.contextMenus.create(menuItem2);
  print(result2);

  // menu item 3
  final menuItem3 = c.CreateContextMenusProperties(
    type: c.ItemType.normal,
    id: 'menu_item_3',
    title: 'menu item 3',
  );
  menuItem3.onClick.listen((data) {
    html.window.alert('Menu item 3 clicked');
  });
  final result3 = await c.chrome.contextMenus.create(menuItem3);
  print(result3);
  final updateProps = new c.UpdateContextMenusProperties(
    type: c.ItemType.normal,
    title: 'menu item 3 updated',
  );
  // update test for menu item 3
  c.chrome.contextMenus.update(menuItem3.id, updateProps);
  updateProps.onClick.listen((data) {
    html.window.alert('Updated menu item 3 clicked');
  });
  // testing actionMenuTopLevelLimit
  final actionMenuTopLevelLimit = c.actionMenuTopLevelLimit;
  if (actionMenuTopLevelLimit == null) {
    print('Expected this to be non null');
  }
}
