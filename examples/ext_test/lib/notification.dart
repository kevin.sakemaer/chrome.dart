import 'package:chrome/chrome.dart' as c;

/// to test notification
void notificationTest() async {
  var btnList = new List<c.Button>();
  btnList.add(new c.Button(title: "Hello", iconUrl: "Dart_Logo_16.png"));
  final opts = c.CreatNotificationOptions(
    type: c.NotificationTemplateType.basic,
    title: 'Title',
    message: 'Message',
    iconUrl: 'Dart_Logo_16.png',
    buttons: btnList,
  );
  final res = await c.chrome.notifications.create('id1', opts);
  print(res);
}
