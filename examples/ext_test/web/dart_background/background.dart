import 'package:chrome_ext_example/context_menus.dart' as c;
import 'package:chrome_ext_example/notification.dart' as n;

void main() async {
  c.contextMenuTest();
  n.notificationTest();
}
